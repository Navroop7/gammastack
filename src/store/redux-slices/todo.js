import { createSlice } from "@reduxjs/toolkit";

const {
  actions: {
    updateTodo,
    updateTodoSuccess,
    updateTodoChild,
    updateTodoChildSuccess,
  },
  reducer,
} = createSlice({
  name: "todo",
  initialState: {
    todoList: [],
  },
  reducers: {
    updateTodo: (state) => ({
      ...state,
    }),
    updateTodoSuccess: (state, { payload }) => ({
      ...state,
      todoList: payload,
    }),
    updateTodoChild: (state) => ({
      ...state,
    }),
    updateTodoChildSuccess: (state, { payload }) => ({
      ...state,
      todoList: payload,
    }),
  },
});

export default reducer;

export {
  updateTodo,
  updateTodoSuccess,
  updateTodoChild,
  updateTodoChildSuccess,
};
