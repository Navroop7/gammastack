import { takeLatest, put } from "redux-saga/effects";

import {
  updateTodo,
  updateTodoSuccess,
  updateTodoChild,
  updateTodoChildSuccess,
} from "../redux-slices/todo";

export default function* todoWatcher() {
  yield takeLatest(updateTodo.type, updateTodoList);
  yield takeLatest(updateTodoChild.type, updateTodoChildList);
}
function* updateTodoList(action) {
  const { tl } = action && action.payload;
  yield put(updateTodoSuccess(tl));
}
function* updateTodoChildList(action) {
  const { userChild, id, todoList } = action && action.payload;

  let result = todoList.filter(function (l) {
    return id === l.id;
  });

  result[0] = {
    ...result[0],
    children: [
      ...result[0].children,
      { value: userChild.value, checked: userChild.checked },
    ],
  };

  const todo = todoList.map((tl) => {
    if (id === tl.id) {
      return result[0];
    } else {
      return tl;
    }
  });

  yield put(updateTodoChildSuccess(todo));
}
