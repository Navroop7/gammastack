import { spawn } from "redux-saga/effects";
import todoWatcher from "./todo";

export default function* rootSaga() {
  yield spawn(todoWatcher);
}
