import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import AddIcon from "@mui/icons-material/Add";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Typography from "@mui/material/Typography";
import Checkbox from "@mui/material/Checkbox";
import { useDispatch, useSelector } from "react-redux";
import { updateTodo, updateTodoChild } from "../store/redux-slices/todo";

function TodoList() {
  const [userInput, setUserInput] = useState("");
  const [userChildInput, setChildUserInput] = useState("");
  const dispatch = useDispatch();
  const { todoList } = useSelector((state) => state.todo);

  const addItem = () => {
    if (userInput !== "") {
      const uInput = {
        id: Math.random(),
        value: userInput,
        children: [],
      };
      const list = [...todoList];
      list.push(uInput);
      dispatch(updateTodo({ tl: list }));
      setUserInput("");
    }
  };
  const addItemChild = (id) => {
    if (userChildInput !== "") {
      const userChild = {
        value: userChildInput,
        checked: false,
      };
      dispatch(updateTodoChild({ userChild, id, todoList }));
      setChildUserInput("");
    }
  };
  const handleChange = (id, e) => {
    let tl;
    const { name, checked } = e.target;
    if (name === "parent") {
      tl = todoList.map((list) => {
        if (list.id !== id) {
          return list;
        }
        return {
          ...list,
          children: list.children.map((child) => {
            return {
              value: child.value,
              checked: checked,
            };
          }),
        };
      });
      dispatch(updateTodo({ tl: tl }));
    } else {
      if (checked) {
        tl = todoList.map((list) => {
          if (list.id !== id) {
            return list;
          } else {
            return {
              ...list,
              children: list.children.map((child) => {
                if (child.value === e.target.value) {
                  return {
                    value: child.value,
                    checked: checked,
                  };
                } else {
                  return {
                    value: child.value,
                    checked: child.checked,
                  };
                }
              }),
            };
          }
        });
        dispatch(updateTodo({ tl: tl }));
      } else {
        tl = todoList.map((list) => {
          if (list.id !== id) {
            return list;
          } else {
            return {
              ...list,
              children: list.children.map((child) => {
                if (child.value === e.target.value) {
                  return {
                    value: child.value,
                    checked: checked,
                  };
                } else {
                  return {
                    value: child.value,
                    checked: child.checked,
                  };
                }
              }),
            };
          }
        });
        dispatch(updateTodo({ tl: tl }));
      }
    }
  };

  return (
    <>
      <div style={{ width: "500px", margin: "auto" }}>
        <TextField
          id="standard-basic"
          label="What to do?"
          variant="standard"
          value={userInput}
          onChange={(e) => {
            setUserInput(e.target.value);
          }}
        />
        <AddIcon
          style={{ marginTop: "20px" }}
          onClick={() => {
            addItem();
          }}
        />
        {todoList.map((item) => {
          return (
            <Accordion>
              <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography>
                  <span>
                    <Checkbox
                      name="parent"
                      onChange={(e) => {
                        handleChange(item.id, e);
                      }}
                      checked={
                        item.children.filter((child) => child.checked !== true)
                          .length < 1
                      }
                    />
                    {item.value}
                  </span>
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                {item.children.map((child) => {
                  return (
                    <Typography>
                      <span>
                        <Checkbox
                          name="child"
                          checked={item.checked || child.checked}
                          onChange={(e) => {
                            handleChange(item.id, e);
                          }}
                          value={child.value}
                        />
                        {child.value}
                      </span>
                    </Typography>
                  );
                })}
                <Typography>
                  <TextField
                    id="standard-basic"
                    label="What are the steps?"
                    variant="standard"
                    onChange={(e) => {
                      setChildUserInput(e.target.value);
                    }}
                  />
                  <AddIcon
                    style={{ marginTop: "20px" }}
                    onClick={() => {
                      addItemChild(item.id);
                    }}
                  />
                </Typography>
              </AccordionDetails>
            </Accordion>
          );
        })}
      </div>
    </>
  );
}

export default TodoList;
