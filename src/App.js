import React from "react";
import TodoList from "./components/TodoList";
import TodoListR from "./components/TodoListR";

function App() {
  return (
    <div style={{ textAlign: "center" }}>
      <h1>Todo App</h1>
      <TodoList />
      <TodoListR />
    </div>
  );
}

export default App;
